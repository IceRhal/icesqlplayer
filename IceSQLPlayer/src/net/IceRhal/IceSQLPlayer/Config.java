package net.IceRhal.IceSQLPlayer;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.Set;
import java.util.logging.Level;

import org.bukkit.Color;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.libs.jline.internal.InputStreamReader;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

public class Config {

    private JavaPlugin plugin;
    private FileConfiguration customConfig;
    private File customConfigFile;
    private String configName;

    public Config(JavaPlugin plugin, String configName) {
    	
        customConfig = null;
        customConfigFile = null;
        this.plugin = plugin;
        this.configName = configName;
        
        reload();
        saveDefault();
    }

    public void reload() {
    	
        if(customConfigFile == null) customConfigFile = new File(plugin.getDataFolder(), configName);

        customConfig = YamlConfiguration.loadConfiguration(customConfigFile);
        Reader defConfigStream;

        try {

            try {

                defConfigStream = new InputStreamReader(plugin.getResource(configName), "UTF-8");
            } catch (NullPointerException e) {

                plugin.saveResource(configName, true);
                return;
            }
        } catch (UnsupportedEncodingException e) {

            defConfigStream = null;
        }

        if(defConfigStream != null) {

            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            customConfig.setDefaults(defConfig);
        }
    }

    public FileConfiguration getConfig() {

        if(customConfig == null) reload();
        return customConfig;
    }
    
    public void save() {

        if(customConfig == null || customConfigFile == null) return;

        try {

            getConfig().save(customConfigFile);
        } catch (IOException | NullPointerException ex) {

            plugin.getLogger().log(Level.SEVERE, ("Could not save config to " + customConfigFile), ex);
        }
    }

    public void saveDefault() {
    	
        if(customConfigFile == null) customConfigFile = new File(plugin.getDataFolder(), configName);

        if(!customConfigFile.exists()) plugin.saveResource(configName, false);

        save();
    }
    
    public <V> Object get(String path, V def) {
    	
    	return customConfig.get(path, def);
    }
    
    public String getString(String path) {
    	
    	return customConfig.getString(path);
    }
    
    public boolean getBoolean(String path) {
    	
    	return customConfig.getBoolean(path);
    }
    
    public int getInt(String path) {
    	
    	return customConfig.getInt(path);
    }
    
    public double getDouble(String path) {
    	
    	return customConfig.getDouble(path);
    }
    
    public long getLong(String path) {
    	
    	return customConfig.getLong(path);
    }
    
    public Color getColor(String path) {
    	
    	return customConfig.getColor(path);
    }
    
    public ItemStack getItemStack(String path) {
    	
    	return customConfig.getItemStack(path);
    }
    
    public OfflinePlayer getOfflinePlayer(String path) {
    	
    	return customConfig.getOfflinePlayer(path);
    }
    
    public Vector getVector(String path) {
    	
    	return customConfig.getVector(path);
    }
    
    public void set(String path, Object value) {
    	    	
    	customConfig.set(path, value);
    }
    
    public Set<String> getKeys() {
    	
    	return customConfig.getKeys(true);
    }
    
    public boolean contains(String path) {
    	
    	return customConfig.contains(path);
    }
}