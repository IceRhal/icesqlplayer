package net.IceRhal.IceSQLPlayer;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

import net.IceRhal.SQL.Column;
import net.IceRhal.SQL.ColumnAttribut;
import net.IceRhal.SQL.ColumnType;
import net.IceRhal.SQL.IntValue;
import net.IceRhal.SQL.MySQL;
import net.IceRhal.SQL.SQL;
import net.IceRhal.SQL.Table;

public class PlayerLoader {
	
	private static FileConfiguration config;
	private static Table t;

	static void setDatabase() {
		
		String path = "mysql.";
		config = IceSQLPlayer.getPlugin().getConfig();
						
		String hostname = config.getString(path + "hostname");
		String port = config.getString(path + "port");
		String database = config.getString(path + "database");
		String username = config.getString(path + "username");
		String password = config.getString(path + "password");
		String tablename = config.getString(path + "tablename");
		
		SQL sql = new SQL(new MySQL(hostname, port, database, username, password));
		
		try {
			
			sql.check();			
		} catch(SQLException e) {
			
			System.out.print("MySQL connection error. Check : hostname, port, database, username and password");
			Bukkit.getPluginManager().disablePlugin(IceSQLPlayer.getPlugin());
		}
		
		List<Column<?>> lc = new ArrayList<Column<?>>();
		
		List<ColumnAttribut> ca = new ArrayList<ColumnAttribut>();
		
		ca.add(ColumnAttribut.AUTO_INCREMENT);
		ca.add(ColumnAttribut.NOT_NULL);
		ca.add(ColumnAttribut.PRIMARY_KEY);
		
		Column<Integer> id = new Column<Integer>("ID", ColumnType.INTEGER, ca);
		
		ca.clear();
		
		ca.add(ColumnAttribut.NOT_NULL);
		
		Column<String> uuid = new Column<String>("UUID", ColumnType.VARCHAR, new IntValue(36), ca);
		Column<String> pData = new Column<String>("PlayerData", ColumnType.TEXT, ca);
		Column<String> pStats = new Column<String>("PlayerStats", ColumnType.TEXT, ca);
		
		lc.add(id);
		lc.add(uuid);
		lc.add(pData);
		lc.add(pStats);

		t = new Table(tablename, lc);		
			
		sql.createTable(t);
	}
	
	@SuppressWarnings("unchecked")
	public static <V> void load(UUID id) {
		
		Map<Column<V>, V> colSel = new HashMap<Column<V>, V>();

		colSel.put((Column<V>) t.getColumn("UUID"), (V) id.toString());		
		
		List<Column<V>> colValue = new ArrayList<Column<V>>();
		
		colValue.add((Column<V>) t.getColumn("PlayerData"));
		colValue.add((Column<V>) t.getColumn("PlayerStats"));
		
		Map<Column<V>, List<V>> map = t.getSQL().getValue(t, colSel, colValue);
				
		String pathData = Bukkit.getWorldContainer() + File.separator + Bukkit.getWorlds().get(0).getName() 
				+ File.separator + "playerdata" + File.separator + id.toString() + ".dat";

		String pathStats = Bukkit.getWorldContainer() + File.separator + Bukkit.getWorlds().get(0).getName() 
				+ File.separator + "stats" + File.separator + id.toString() + ".json";
						
		try {
			
			Utils.decode((String) map.get(t.getColumn("PlayerData")).get(0), pathData);	
			Utils.decode((String) map.get(t.getColumn("PlayerStats")).get(0), pathStats);			
		} catch(Exception e) {
			
			if(e instanceof IndexOutOfBoundsException) return;
			e.printStackTrace();
		}		
	}
	
	@SuppressWarnings("unchecked")
	public static <V> void save(UUID id) {
		
		Map<Column<V>, V> colSel = new HashMap<Column<V>, V>();
		Map<Column<V>, V> colChange = new HashMap<Column<V>, V>();

		colChange.put((Column<V>) t.getColumn("UUID"), (V) id.toString());	
		
		String pathData = Bukkit.getWorldContainer() + File.separator + Bukkit.getWorlds().get(0).getName() 
				+ File.separator + "playerdata" + File.separator + id.toString() + ".dat";
		
		String pathStats = Bukkit.getWorldContainer() + File.separator + Bukkit.getWorlds().get(0).getName() 
				+ File.separator + "stats" + File.separator + id.toString() + ".json";
				
		try {
			
			String b64Data = Utils.encode(pathData);
			String b64Stats = Utils.encode(pathStats);
									
			colChange.put((Column<V>) t.getColumn("PlayerData"), (V) b64Data);
			colChange.put((Column<V>) t.getColumn("PlayerStats"), (V) b64Stats);
			
			if(getId(id) != -1) colSel.put((Column<V>) t.getColumn("ID"), (V) getId(id));
			else colSel.put((Column<V>) t.getColumn("ID"), (V) (Integer) 0);
			
			t.getSQL().setValue(t, colSel, colChange);
			
		} catch(Exception e) {
			
			if(!e.getMessage().equalsIgnoreCase("No column exit")) e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	static <V> Integer getId(UUID id) {
		
		Map<Column<V>, V> colSel = new HashMap<Column<V>, V>();

		colSel.put((Column<V>) t.getColumn("UUID"), (V) id.toString());		
		
		List<Integer> ls = t.getSQL().getValue(t, colSel, (Column<Integer>) t.getColumn("ID"));
		
		if(ls == null || ls.isEmpty()) return -1;
		else return ls.get(0);
	}
}
