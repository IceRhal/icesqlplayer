package net.IceRhal.IceSQLPlayer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;

public class Utils {
	
	public static String encode(String sourceFilePath) throws Exception {	    	 
       
		byte[] base64EncodedData = Base64.encodeBase64(loadFileAsBytesArray(sourceFilePath));	 
	        
        return Base64.encodeBase64String(base64EncodedData);	        
	}   
	
	public static File decode(String base64, String filePath) throws Exception {	 
	       
		byte[] decodedBytes = Base64.decodeBase64(Base64.decodeBase64(base64));
	       
		return writeByteArraysToFile(filePath, decodedBytes);	   
	}	    
	    
	private static byte[] loadFileAsBytesArray(String fileName) throws Exception {	    	 
	       
		File file = new File(fileName);
	    
		int length = (int) file.length();
	    
		BufferedInputStream reader = new BufferedInputStream(new FileInputStream(file));
	    
		byte[] bytes = new byte[length];
	    
		reader.read(bytes, 0, length);	    
		reader.close();
	    
		return bytes;	    
	}
	
	private static File writeByteArraysToFile(String fileName, byte[] content) throws IOException {		
	    
		File file = new File(fileName);
		
		if(!file.exists()) {
			
			if(!file.getParentFile().exists()) file.getParentFile().mkdirs();
			file.createNewFile();
		}
	    
		BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
	    
		writer.write(content);	    
		writer.flush();	    
		writer.close();		
	    
		return file;		
	}
}

