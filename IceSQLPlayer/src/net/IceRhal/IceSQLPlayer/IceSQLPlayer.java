package net.IceRhal.IceSQLPlayer;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class IceSQLPlayer extends JavaPlugin {
	
	private static IceSQLPlayer plugin;
	private Config pLoc;
	
	public void onEnable() {
		
		plugin = this;
		
		loadConfig();
		PlayerLoader.setDatabase();
		
		if(plugin.isEnabled() && getConfig().getBoolean("enabled")) Bukkit.getPluginManager().registerEvents(new Events(), plugin);
	}
	
	public static IceSQLPlayer getPlugin() {
		
		return plugin;
	}
	
	private void loadConfig() {
		
		saveDefaultConfig();
		
		saveConfig();
		
		pLoc = new Config(this, "players.yml");
	}
	
	Config getPLoc() {
		
		return pLoc;
	}
}
