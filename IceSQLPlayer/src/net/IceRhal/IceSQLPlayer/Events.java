package net.IceRhal.IceSQLPlayer;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class Events implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void AsyncPlayerPreLogin(final AsyncPlayerPreLoginEvent e) {

		final IceSQLPlayer plugin = IceSQLPlayer.getPlugin();

		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

			@Override
			public void run() {

				PlayerLoader.load(e.getUniqueId());

				Player p = Bukkit.getPlayer(e.getUniqueId());

				String d = "org.bukkit.craftbukkit.";
				String f = ".entity.CraftPlayer";

				try {

					Class.forName(d + "v1_9_R1" + f);

					((org.bukkit.craftbukkit.v1_9_R1.entity.CraftPlayer) p).getHandle().getStatisticManager().a();
				} catch(ClassNotFoundException e4) {

					try {

						Class.forName(d + "v1_8_R3" + f);

						((org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer) p).getHandle().getStatisticManager().a();
					} catch(ClassNotFoundException e0) {

						try {

							Class.forName(d + "v1_7_R2" + f);

							((org.bukkit.craftbukkit.v1_7_R2.entity.CraftPlayer) p).getHandle().getStatisticManager()
									.a();
						} catch(ClassNotFoundException e1) {

							try {

								Class.forName(d + "v1_8_R2" + f);

								((org.bukkit.craftbukkit.v1_8_R2.entity.CraftPlayer) p).getHandle()
										.getStatisticManager().a();
							} catch(ClassNotFoundException e2) {

								try {

									Class.forName(d + "v1_8_R1" + f);

									((org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer) p).getHandle()
											.getStatisticManager().a();
								} catch(ClassNotFoundException e3) {

									System.out.print("Bukkit version not supported.");
									e3.printStackTrace();
								}
							}
						}
					}
				}

				p.loadData();

				GameMode gm = p.getGameMode();

				if(!gm.equals(GameMode.CREATIVE)) p.setGameMode(GameMode.CREATIVE);
				else p.setGameMode(GameMode.SPECTATOR);

				p.setGameMode(gm);

				Location l = Bukkit.getWorlds().get(0).getSpawnLocation();

				String locType = plugin.getConfig().getString("respawn_location");

				if(locType.equalsIgnoreCase("last")) {

					Config pLoc = plugin.getPLoc();

					String path = p.getUniqueId() + ".";

					if(pLoc.contains(path + "world")) l = new Location(Bukkit.getWorld(pLoc.getString(path + "world")),
							pLoc.getDouble(path + "x"), pLoc.getDouble(path + "y"), pLoc.getDouble(path + "z"),
							pLoc.getLong(path + "yaw"), pLoc.getLong(path + "pitch"));
				} else if(locType.equalsIgnoreCase("custom")) {

					FileConfiguration c = plugin.getConfig();

					String path = "customLoc.";

					if(c.contains(path + "world")) l = new Location(Bukkit.getWorld(c.getString(path + "world")),
							c.getDouble(path + "x"), c.getDouble(path + "y"), c.getDouble(path + "z"),
							c.getLong(path + "yaw"), c.getLong(path + "pitch"));
				} else if(locType.equalsIgnoreCase("default")) l = p.getLocation();

				if(l == null) l = Bukkit.getWorlds().get(0).getSpawnLocation();

				p.teleport(l);
			}
		}, 20L);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void PlayerQuitEvent(PlayerQuitEvent e) {

		Player p = e.getPlayer();

		p.saveData();

		Config pLoc = IceSQLPlayer.getPlugin().getPLoc();

		String path = p.getUniqueId() + ".";

		Location l = p.getLocation();

		pLoc.set(path + "world", l.getWorld().getName());
		pLoc.set(path + "x", l.getX());
		pLoc.set(path + "y", l.getY());
		pLoc.set(path + "z", l.getZ());
		pLoc.set(path + "yaw", l.getYaw());
		pLoc.set(path + "pitch", l.getPitch());

		pLoc.save();

		PlayerLoader.save(p.getUniqueId());
	}
}
